import React, { useState, useEffect } from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";

import CheckoutForm from "./CheckoutForm";
import "./App.css";

// Make sure to call loadStripe outside of a component’s render to avoid
// recreating the Stripe object on every render.
// This is your test publishable API key.
const stripePromise = loadStripe("pk_test_51KJP9wEKTg0TVwmrM0tqW4TR2ebYKJ6sWnLRfUph8YrGU9HjBRnL90RUjbKUXY2h3CemtuCotOwFAXORkMKgghhk007AmRZOOZ");

export default function App() {
  const [clientSecret, setClientSecret] = useState("");

  useEffect(() => {
    // Create PaymentIntent as soon as the page loads
    fetch("/create-payment-intent", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ items: [{ id: "xl-tshirt" }] }),
    })
      .then((res) => res.json())
      .then((data) => setClientSecret(data.clientSecret));
  }, []);

  const appearance = {
    theme: 'none',
    labels: 'floating',
    rules: {
      '.Input': {
        backgroundColor: '#EEEFF0',
        borderRadius: '10px 10px 0px 0px',
        borderBottom: '2px solid #2B3949'
      },
      '.Input:focus': {
        borderBottom: '2px solid #ca4004',
        outline: 'none',
      },
      '.Label': {
      }
    }
  };
  const options = {
    clientSecret,
    appearance,
  };

  return (
    <div className="App">
      {clientSecret && (
        <Elements options={options} stripe={stripePromise}>
          <CheckoutForm />
        </Elements>
      )}
    </div>
  );
}
