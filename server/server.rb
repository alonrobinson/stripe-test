require 'stripe'
require 'dotenv'
require 'sinatra'

Dotenv.load
Stripe.api_key = ENV['STRIPE_SECRET_KEY']

set :static, true
set :public_folder, "../client"
set :port, 4242

get '/' do
  content_type "text/html"
  send_file File.join(settings.public_folder, "index.html")
end

get '/public-key' do
  content_type 'application/json'
  { key: ENV['STRIPE_PUBLIC_KEY'] }.to_json
end


# An endpoint to start the payment process
post '/create-payment-intent' do
  content_type 'application/json'
  data = JSON.parse(request.body.read)

  # Create a PaymentIntent with amount and currency
  payment_intent = Stripe::PaymentIntent.create(
    amount: calculate_order_amount(data['items']),
    currency: 'eur',
    automatic_payment_methods: {
      enabled: true,
    },
  )

  {
    clientSecret: payment_intent['client_secret']
  }.to_json
end

def calculate_order_amount(_items)
  # Replace this constant with a calculation of the order's amount
  # Calculate the order total on the server to prevent
  # people from directly manipulating the amount on the client
  1400
end
